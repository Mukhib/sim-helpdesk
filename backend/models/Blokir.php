<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "blokir".
 *
 * @property integer $id_blokir
 * @property integer $biller_id_bil
 * @property integer $loket_id_lok
 * @property string $tanggal_waktu
 * @property string $status
 */
class Blokir extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blokir';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['biller_id_bil', 'loket_id_lok', 'tanggal_waktu', 'status'], 'required'],
            [['biller_id_bil', 'loket_id_lok'], 'integer'],
            [['tanggal_waktu'], 'safe'],
            [['status'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_blokir' => 'Id Blokir',
            'biller_id_bil' => 'Biller Id Bil',
            'loket_id_lok' => 'Loket Id Lok',
            'tanggal_waktu' => 'Tanggal Waktu',
            'status' => 'Status',
        ];
    }

    public function getBiller()
    {
      return $this->hasOne(Biller::className(),['id_biller' => 'biller_id_bil']);
    }

    public function getLoket()
    {
      return $this->hasOne(Loket::className(),['id_loket' => 'loket_id_lok']);
    }

}
