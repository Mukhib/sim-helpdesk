<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Sop;

/**
 * SopSearch represents the model behind the search form about `backend\models\Sop`.
 */
class SopSearch extends Sop
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_sop', 'master_komplain_id_mk'], 'integer'],
            [['tanggal_waktu', 'isi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sop::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_sop' => $this->id_sop,
            'master_komplain_id_mk' => $this->master_komplain_id_mk,
            'tanggal_waktu' => $this->tanggal_waktu,
        ]);

        $query->andFilterWhere(['like', 'isi', $this->isi]);

        return $dataProvider;
    }
}
