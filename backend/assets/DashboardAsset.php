<?php 
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
        'css/ionicons.min.css',
        'css/AdminLTE.min.css',
        'css/skins/_all-skins.min.css',
        'plugins/iCheck/flat/blue.css',
        'css/site.css',
    ];
    public $js = [
      'js/bootstrap/bootstrap.min.js',
      'js/jquery-ui.min.js',
      'js/raphael-min.js',
      'plugins/sparkline/jquery.sparkline.min.js',
      'plugins/slimScroll/jquery.slimscroll.min.js',
      'plugins/fastclick/fastclick.min.js',
      'js/app.min.js',
      'js/dasboard.js',
      'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
