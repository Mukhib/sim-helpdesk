<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Blokir */

$this->title = $model->id_blokir;
$this->params['breadcrumbs'][] = ['label' => 'Blokirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blokir-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_blokir], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_blokir], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_blokir',
            'biller_id_bil',
            'loket_id_lok',
            'tanggal_waktu',
            'status',
        ],
    ]) ?>

</div>
