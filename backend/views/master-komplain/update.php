<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MasterKomplain */

$this->title = 'Update Master Komplain: ' . $model->id_master_kom;
$this->params['breadcrumbs'][] = ['label' => 'Master Komplains', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_master_kom, 'url' => ['view', 'id' => $model->id_master_kom]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-komplain-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
