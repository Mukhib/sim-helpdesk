<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\models\MasterKomplain;

/* @var $this yii\web\View */
/* @var $model backend\models\Sop */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sop-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'master_komplain_id_mk')->dropDownList(
            ArrayHelper::map(MasterKomplain::find()->all(),'id_master_kom','nama_komplain'),
            ['promt'=>'Pilih komplain']
      ) ?>

    <?= $form->field($model, 'isi')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
