<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Sop */

$this->title = 'Update Sop: ' . $model->id_sop;
$this->params['breadcrumbs'][] = ['label' => 'Sops', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_sop, 'url' => ['view', 'id' => $model->id_sop]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sop-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
