<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\KomplainSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="komplain-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_komplain') ?>

    <?= $form->field($model, 'loket_id_lok') ?>

    <?= $form->field($model, 'master_komplain_id_mk') ?>

    <?= $form->field($model, 'user_id_usr') ?>

    <?= $form->field($model, 'catatan') ?>

    <?php // echo $form->field($model, 'tanggal_waktu_datang') ?>

    <?php // echo $form->field($model, 'waktu_pengerjaaan') ?>

    <?php // echo $form->field($model, 'waktu_selesai') ?>

    <?php // echo $form->field($model, 'solusi_penanganan') ?>

    <?php // echo $form->field($model, 'lama_pengerjaan') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
