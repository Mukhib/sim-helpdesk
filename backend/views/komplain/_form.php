<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\models\Loket;
use backend\models\MasterKomplain;
use backend\models\User;
/* @var $this yii\web\View */
/* @var $model backend\models\Komplain */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="komplain-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'loket_id_lok')->dropDownList(
            ArrayHelper::map(Loket::find()->all(),'id_loket','nama_loket'),
            ['promt'=>'Pilih loket']
      ) ?>

    <?= $form->field($model, 'master_komplain_id_mk')->dropDownList(
            ArrayHelper::map(MasterKomplain::find()->all(),'id_master_kom','nama_komplain'),
            ['promt'=>'Pilih komplain']
      ) ?>

    <?= $form->field($model, 'user_id_usr')->dropDownList(
            ArrayHelper::map(User::find()->all(),'id','username'),
            ['promt'=>'Pilih user']
      ) ?>

    <?= $form->field($model, 'catatan')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
