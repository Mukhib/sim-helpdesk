<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Loket */

$this->title = 'Update Loket: ' . $model->id_loket;
$this->params['breadcrumbs'][] = ['label' => 'Lokets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_loket, 'url' => ['view', 'id' => $model->id_loket]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="loket-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
