<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LoketSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loket-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_loket') ?>

    <?= $form->field($model, 'nama_loket') ?>

    <?= $form->field($model, 'penanggung_jawab') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'no_kontak') ?>

    <?php // echo $form->field($model, 'alamat') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
