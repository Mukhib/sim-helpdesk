<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Biller */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="biller-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_biller')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fitur')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_kontak')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
