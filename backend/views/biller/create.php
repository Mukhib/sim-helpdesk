<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Biller */

$this->title = 'Tambah Biller';
$this->params['breadcrumbs'][] = ['label' => 'Billers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biller-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
